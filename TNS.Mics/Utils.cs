﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace TNS.Misc
{
    public static class Utils
    {
        #region [Paint GV]
        private static Color _GVHoverColor = Color.LightGreen;  // = Color.LightGreen;//For hover backcolor
        private static Color _GVbackColor = Color.FromArgb(181, 213, 255);   //For backColor     Color.LightSkyBlue; // 
        public static Point _hotSpot;

        public static void DrawGVStyle(ref DataGridView GV)
        {
            GV.ClipboardCopyMode = DataGridViewClipboardCopyMode.Disable;
            GV.BackgroundColor = Color.White;
            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Black;
            GV.EnableHeadersVisualStyles = false;
            GV.RowHeadersVisible = false;
            GV.AllowUserToResizeRows = false;
            GV.AllowUserToResizeColumns = true;
            GV.AllowUserToDeleteRows = false;
            GV.MultiSelect = false;
            GV.ColumnHeadersHeight = 25;
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);

            foreach (DataGridViewColumn column in GV.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            if (GV.Columns.Count > 0)
            {
                for (int i = 1; i <= 8; i++)
                {
                    GV.Rows.Add();
                }
            }
            GV.CellPainting += GV_CellPainting;
            GV.MouseMove += GV_MouseMove;
            GV.MouseLeave += GV_MouseLeave;
        }

        public static void GV_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            DataGridView GV = (DataGridView)sender;
            if (e.RowIndex == -1 && e.ColumnIndex > -1)
            {
                e.PaintBackground(e.CellBounds, true);
                RenderColumnHeader(e.Graphics, e.CellBounds, e.CellBounds.Contains(_hotSpot) ? _GVHoverColor : _GVbackColor);
                RenderColumnHeaderBorder(GV, e.Graphics, e.CellBounds, e.ColumnIndex);
                using (Brush brush = new SolidBrush(e.CellStyle.ForeColor))
                {
                    using (StringFormat sf = new StringFormat() { LineAlignment = StringAlignment.Center, Alignment = StringAlignment.Center })
                    {
                        e.Graphics.DrawString(e.Value.ToString(), new Font("Tahoma", 9, FontStyle.Bold), brush, e.CellBounds, sf);
                    }
                }
                e.Handled = true;
            }
        }

        public static void RenderColumnHeader(Graphics g, Rectangle headerBounds, Color c)
        {
            int topHeight = 10;
            Rectangle topRect = new Rectangle(headerBounds.Left, headerBounds.Top + 1, headerBounds.Width, topHeight);
            RectangleF bottomRect = new RectangleF(headerBounds.Left, headerBounds.Top + 1 + topHeight, headerBounds.Width, headerBounds.Height - topHeight - 4);
            Color c1 = Color.FromArgb(180, c);
            using (SolidBrush brush = new SolidBrush(c1))
            {
                g.FillRectangle(brush, topRect);
                brush.Color = c;
                g.FillRectangle(brush, bottomRect);
            }
        }
        public static void RenderColumnHeaderBorder(DataGridView GV, Graphics g, Rectangle headerBounds, int colIndex)
        {
            g.DrawRectangle(new Pen(Color.White, 0.1f), headerBounds.Left + 0.5f, headerBounds.Top + 0.5f, headerBounds.Width - 1f, headerBounds.Height - 1f);
            ControlPaint.DrawBorder(g, headerBounds,
                Color.Gray, 0, ButtonBorderStyle.Solid,
                Color.Gray, 0, ButtonBorderStyle.Solid,
                Color.Gray, colIndex != GV.ColumnCount - 1 ? 1 : 0, ButtonBorderStyle.Solid,
                Color.Gray, 1, ButtonBorderStyle.Solid);
        }
        //MouseMove event handler for your GV
        public static void GV_MouseMove(object sender, MouseEventArgs e)
        {
            _hotSpot = e.Location;
        }
        //MouseLeave event handler for your GV
        public static void GV_MouseLeave(object sender, EventArgs e)
        {
            _hotSpot = Point.Empty;
        }
        #endregion

        #region [Paint LV]
        private static Color _LVForceColor = Color.Navy;                // = Color.LightGreen;//For hover backcolor
        private static Color _LVbackColor = Color.FromArgb(181, 213, 255);// = Color.LightSkyBlue;    //For backColor    

        public static void DrawLVStyle(ref ListView list)
        {
            list.OwnerDraw = true;

            list.DrawColumnHeader +=
                new DrawListViewColumnHeaderEventHandler
                (
                    (sender, e) => headerDraw(sender, e, _LVbackColor, _LVForceColor)
                );
            list.DrawItem += new DrawListViewItemEventHandler(bodyDraw);
        }

        private static void headerDraw(object sender, DrawListViewColumnHeaderEventArgs e, Color backColor, Color foreColor)
        {
            ListView lv = (ListView)sender;

            int topHeight = 10;
            Rectangle topRect = new Rectangle(e.Bounds.Left, e.Bounds.Top + 1, e.Bounds.Width, topHeight);
            RectangleF bottomRect = new RectangleF(e.Bounds.Left, e.Bounds.Top + 1 + topHeight, e.Bounds.Width, e.Bounds.Height - topHeight - 4);
            Color c1 = Color.FromArgb(180, backColor);
            using (SolidBrush brush = new SolidBrush(c1))
            {
                e.Graphics.FillRectangle(brush, topRect);
                brush.Color = backColor;
                e.Graphics.FillRectangle(brush, bottomRect);
            }

            e.Graphics.DrawRectangle(new Pen(Color.White, 0.1f), e.Bounds.Left + 0.5f, e.Bounds.Top + 0.5f, e.Bounds.Width - 1f, e.Bounds.Height - 1f);
            ControlPaint.DrawBorder(e.Graphics, e.Bounds,
                Color.Gray, 0, ButtonBorderStyle.Solid,
                Color.Gray, 0, ButtonBorderStyle.Solid,
                Color.Gray, e.ColumnIndex != lv.Columns.Count - 1 ? 1 : 0, ButtonBorderStyle.Solid,
                Color.Gray, 1, ButtonBorderStyle.Solid);

            using (SolidBrush foreBrush = new SolidBrush(foreColor))
            {
                using (StringFormat sf = new StringFormat() { LineAlignment = StringAlignment.Center, Alignment = StringAlignment.Center })
                {
                    e.Graphics.DrawString(e.Header.Text, new Font("Tahoma", 9, FontStyle.Bold), foreBrush, e.Bounds, sf);

                }

            }
        }

        private static void bodyDraw(object sender, DrawListViewItemEventArgs e)
        {
            e.DrawDefault = true;
            
        }

        #endregion
        /// <summary>
        /// 
        /// </summary>
        /// <param name="title">tiêu đề lỗi</param>
        /// <param name="description">nội dung lỗi</param>
        /// <param name="type">1 thông tin, 2 cảnh báo, 3 thành công, 4 lỗi</param>
        /// <returns>Y hoặc là N</returns>
        public static string TNMessageBox(string description, int type)
        {
            // using construct ensures the resources are freed when form is closed
            using (var form = new TNMessageBox(description, type))
            {
                form.ShowDialog();
                if (form.ActionResult == 1)
                    return "Y";
                else
                    return "N";
            }
        }
        public static void TNMessageBoxOK(string description, int type)
        {
            // using construct ensures the resources are freed when form is closed
            using (var form = new TNMessageBoxOK(description, type))
            {
                form.ShowDialog();
            }
        }
        public static void SizeLastColumn_LV(ListView lv)
        {
            if (lv.Columns.Count > 0)
                lv.Columns[lv.Columns.Count - 1].Width = -2;
        }

        public static void DoubleBuffered(DataGridView dgv, bool setting)
        {
            Type dgvType = dgv.GetType();
            PropertyInfo pi = dgvType.GetProperty("DoubleBuffered",
                BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.SetProperty);
            pi.SetValue(dgv, setting, null);
        }
        public static string FirstCharToUpper(this string input)
        {
            if (String.IsNullOrEmpty(input))
            {
                return string.Empty;
            }

            return input.First().ToString().ToUpper() + input.Substring(1);
        }
        public static string NumberToWordsEN(long lNumber)
        {

            string[] ones = {"One ","Two ","Three ","Four ","Five ","Six ","Seven ","Eight ","Nine ","Ten ",
                              "Eleven ","Twelve ","Thirteen ","Fourteen ","Fifteen ","Sixteen ","Seventeen ","Eighteen ","Ninteen "
                            };
            string[] tens = { "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninty " };

            if (lNumber == 0)
            {
                return ("");
            }

            if (lNumber < 0)
            {

                lNumber *= -1;
            }
            if (lNumber < 20)
            {
                return ones[lNumber - 1];
            }
            if (lNumber <= 99)
            {
                return tens[(lNumber / 10) - 2] + NumberToWordsEN(lNumber % 10);
            }
            if (lNumber < 1000)
            {
                return NumberToWordsEN(lNumber / 100) + "Hundred " + NumberToWordsEN(lNumber % 100);
            }
            if (lNumber < 100000)
            {
                return NumberToWordsEN(lNumber / 1000) + "Thousand " + NumberToWordsEN(lNumber % 1000);
            }
            if (lNumber < 10000000)
            {
                return NumberToWordsEN(lNumber / 100000) + "Lakh " + NumberToWordsEN(lNumber % 100000);
            }
            if (lNumber < 1000000000)
            {
                return NumberToWordsEN(lNumber / 10000000) + "Crore " + NumberToWordsEN(lNumber % 10000000);
            }
            return "";
        }
        public static string NumberToWordsVN(double number, bool IsMoney)
        {
            string s = number.ToString("#");
            string[] so = new string[] { "không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín" };
            string[] hang = new string[] { "", "nghìn", "triệu", "tỷ" };
            int i, j, donvi, chuc, tram;
            string str = " ";
            bool booAm = false;
            decimal decS = 0;
            //Tung addnew
            try
            {
                decS = Convert.ToDecimal(s.ToString());
            }
            catch
            {
            }
            if (decS < 0)
            {
                decS = -decS;
                s = decS.ToString();
                booAm = true;
            }
            i = s.Length;
            if (i == 0)
            {
                str = so[0] + str;
            }
            else
            {
                j = 0;
                while (i > 0)
                {
                    donvi = Convert.ToInt32(s.Substring(i - 1, 1));
                    i--;
                    if (i > 0)
                    {
                        chuc = Convert.ToInt32(s.Substring(i - 1, 1));
                    }
                    else
                    {
                        chuc = -1;
                    }

                    i--;
                    if (i > 0)
                    {
                        tram = Convert.ToInt32(s.Substring(i - 1, 1));
                    }
                    else
                    {
                        tram = -1;
                    }

                    i--;
                    if ((donvi > 0) || (chuc > 0) || (tram > 0) || (j == 3))
                    {
                        str = hang[j] + str;
                    }

                    j++;
                    if (j > 3)
                    {
                        j = 1;
                    }

                    if ((donvi == 1) && (chuc > 1))
                    {
                        str = "một " + str;
                    }
                    else
                    {
                        if ((donvi == 5) && (chuc > 0))
                        {
                            str = "lăm " + str;
                        }
                        else if (donvi > 0)
                        {
                            str = so[donvi] + " " + str;
                        }
                    }
                    if (chuc < 0)
                    {
                        break;
                    }
                    else
                    {
                        if ((chuc == 0) && (donvi > 0))
                        {
                            str = "lẻ " + str;
                        }

                        if (chuc == 1)
                        {
                            str = "mười " + str;
                        }

                        if (chuc > 1)
                        {
                            str = so[chuc] + " mươi " + str;
                        }
                    }
                    if (tram < 0)
                    {
                        break;
                    }
                    else
                    {
                        if ((tram > 0) || (chuc > 0) || (donvi > 0))
                        {
                            str = so[tram] + " trăm " + str;
                        }
                    }
                    str = " " + str;
                }
            }
            if (booAm)
            {
                str = "Âm " + str;
            }
            if (IsMoney)
            {
                return str.Trim() + " " + "đồng chẵn";
            }
            else
            {
                return str.Trim();
            }
        }
        //to number
        public static int ToInt(this object obj)
        {
            try
            {
                return int.Parse(obj.ToString());
            }
            catch
            {
                return 0;
            }
        }
        public static float ToFloat(this object obj)
        {
            try
            {
                if (obj.ToString().Contains(","))
                {
                    obj = obj.ToString().Replace(",", "");
                }

                return float.Parse(obj.ToString());
            }
            catch
            {
                return 0;
            }
        }
        public static decimal ToDecimal(this object obj)
        {
            try
            {
                if (obj.ToString().Contains(","))
                {
                    obj = obj.ToString().Replace(",", "");
                }

                return decimal.Parse(obj.ToString());
            }
            catch
            {
                return 0;
            }
        }
        public static double ToRound(this object obj)
        {
            try
            {
                if (obj.ToString().Contains(","))
                {
                    obj = obj.ToString().Replace(",", "");
                }

                return Math.Round(double.Parse(obj.ToString()), 1);
            }
            catch
            {
                return 0;
            }
        }
        public static double ToDouble(this object obj)
        {
            try
            {
                if (obj.ToString().Contains(","))
                {
                    obj = obj.ToString().Replace(",", "");
                }

                return double.Parse(obj.ToString());
            }
            catch
            {
                return 0;
            }
        }
        //to string
        public static string Ton0String(this object obj)
        {
            try
            {
                return double.Parse(obj.ToString()).ToString("n0");
            }
            catch
            {
                return "0";
            }
        }
        public static string Ton1String(this object obj)
        {
            try
            {
                return double.Parse(obj.ToString()).ToString("n1");
            }
            catch
            {
                return "0";
            }
        }

        public static string Ton2String(this object obj)
        {
            try
            {
                return double.Parse(obj.ToString()).ToString("n2");
            }
            catch
            {
                return "0";
            }
        }
        public static string Toe0String(this object obj)
        {
            try
            {
                if (double.Parse(obj.ToString()) != 0)
                    return double.Parse(obj.ToString()).ToString("n0");
                else
                    return string.Empty;
            }
            catch
            {
                return "";
            }
        }
        public static string Toe1String(this object obj)
        {
            try
            {
                if (double.Parse(obj.ToString()) != 0)
                {
                    return double.Parse(obj.ToString()).ToString("n1");
                }
                else
                    return string.Empty;
            }
            catch
            {
                return "";
            }
        }
        public static string Toe2String(this object obj)
        {
            try
            {
                if (double.Parse(obj.ToString()) != 0)
                    return double.Parse(obj.ToString()).ToString("n2");
                else
                    return string.Empty;
            }
            catch
            {
                return "";
            }
        }
        public static string ToViString(this object obj)
        {
            return double.Parse(obj.ToString()).ToString("##,##0", new CultureInfo("vi-VN"));
        }
        /// <summary>
        /// parse datetime to dd/MM/yyyy
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToDateString(this object obj)
        {
            try
            {
                string s = "";
                DateTime zDate = DateTime.Parse(obj.ToString());
                if (zDate != DateTime.MinValue)
                {
                    s = zDate.ToString("dd/MM/yyyy");
                }

                return s;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        public static String ToAscii(this String unicode)
        {

            unicode = unicode.ToLower().Trim();
            unicode = Regex.Replace(unicode, "[áàảãạăắằẳẵặâấầẩẫậ]", "a");
            unicode = Regex.Replace(unicode, "[๖ۣۜ]", "");
            unicode = Regex.Replace(unicode, "[óòỏõọôồốổỗộơớờởỡợ]", "o");
            unicode = Regex.Replace(unicode, "[éèẻẽẹêếềểễệ]", "e");
            unicode = Regex.Replace(unicode, "[íìỉĩị]", "i");
            unicode = Regex.Replace(unicode, "[úùủũụưứừửữự]", "u");
            unicode = Regex.Replace(unicode, "[ýỳỷỹỵ]", "y");
            unicode = Regex.Replace(unicode, "[đ]", "d");
            unicode = unicode.Replace(" ", "-").Replace("[()]", "");
            unicode = Regex.Replace(unicode, "[-\\s+/]+", "-");
            unicode = Regex.Replace(unicode, "\\W+", "-"); //Nếu bạn muốn thay dấu khoảng trắng thành dấu "_" hoặc dấu cách " " thì thay kí tự bạn muốn vào đấu "-"
            return unicode;
        }
        /// <summary>
        /// Converts a DataTable to a list with generic objects
        /// </summary>
        /// <typeparam name="T">Generic object</typeparam>
        /// <param name="table">DataTable</param>
        /// <returns>List with generic objects</returns>
        public static List<T> DataTableToList<T>(this DataTable table) where T : class, new()
        {
            try
            {
                List<T> list = new List<T>();

                foreach (var row in table.AsEnumerable())
                {
                    T obj = new T();

                    foreach (var prop in obj.GetType().GetProperties())
                    {
                        try
                        {
                            PropertyInfo propertyInfo = obj.GetType().GetProperty(prop.Name);
                            propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);
                        }
                        catch
                        {
                            continue;
                        }
                    }

                    list.Add(obj);
                }

                return list;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Tính lấy năm
        /// </summary>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public static int GetTotalYear(DateTime FromDate, DateTime ToDate)
        {
            DateTime zeroTime = new DateTime(1, 1, 1);

            DateTime a = FromDate;
            DateTime b = ToDate;

            TimeSpan span = b - a;
            // Because we start at year 1 for the Gregorian
            // calendar, we must subtract a year here.
            int years = (zeroTime + span).Year - 1;

            // 1, where my other algorithm resulted in 0.
            return years;
        }

        public static string TimeAgo(DateTime FromDate, DateTime ToDate)
        {
            if (FromDate > ToDate)
                return "about sometime from now";
            TimeSpan span = ToDate - FromDate;

            if (span.Days > 365)
            {
                int years = (span.Days / 365);
                if (span.Days % 365 != 0)
                    years += 1;
                return String.Format("about {0} {1} ago", years, years == 1 ? "year" : "years");
            }

            if (span.Days > 30)
            {
                int months = (span.Days / 30);
                if (span.Days % 31 != 0)
                    months += 1;
                return String.Format("about {0} {1} ago", months, months == 1 ? "month" : "months");
            }

            if (span.Days > 0)
                return String.Format("about {0} {1} ago", span.Days, span.Days == 1 ? "day" : "days");

            if (span.Hours > 0)
                return String.Format("about {0} {1} ago", span.Hours, span.Hours == 1 ? "hour" : "hours");

            if (span.Minutes > 0)
                return String.Format("about {0} {1} ago", span.Minutes, span.Minutes == 1 ? "minute" : "minutes");

            if (span.Seconds > 5)
                return String.Format("about {0} seconds ago", span.Seconds);

            if (span.Seconds <= 5)
                return "just now";

            return string.Empty;
        }
        public static string CalculatorTime(DateTime FromDate, DateTime ToDate)
        {

            int Years = new DateTime(ToDate.Subtract(FromDate).Ticks).Year - 1;
            DateTime _DOBDateNow = FromDate.AddYears(Years);
            int Months = 0;
            for (int i = 1; i <= 12; i++)
            {
                if (_DOBDateNow.AddMonths(i) == ToDate)
                {
                    Months = i;
                    break;
                }
                else if (_DOBDateNow.AddMonths(i) >= ToDate)
                {
                    Months = i - 1;
                    break;
                }
            }
            int Days = ToDate.Subtract(_DOBDateNow.AddMonths(Months)).Days;
            return "" + Years + " năm, " + Months + " tháng, " + Days + " ngày.";
        }
        public static string CalculateYearOld(DateTime FromDate, DateTime ToDate)
        {

            int Years = new DateTime(ToDate.Subtract(FromDate).Ticks).Year - 1;
            DateTime _DOBDateNow = FromDate.AddYears(Years);
            int Months = 0;
            for (int i = 1; i <= 12; i++)
            {
                if (_DOBDateNow.AddMonths(i) == ToDate)
                {
                    Months = i;
                    break;
                }
                else if (_DOBDateNow.AddMonths(i) >= ToDate)
                {
                    Months = i - 1;
                    break;
                }
            }
            return Years.ToString();
            //return $"Age is {_Years} Years {_Months} Months {Days} Days";
        }
        //public static string CalculateYourTime(DateTime FromDate, DateTime ToDate, out int YearNum)
        //{

        //    int Years = new DateTime(ToDate.Subtract(FromDate).Ticks).Year - 1;
        //    DateTime _DOBDateNow = FromDate.AddYears(Years);
        //    int Months = 0;
        //    for (int i = 1; i <= 12; i++)
        //    {
        //        if (_DOBDateNow.AddMonths(i) == ToDate)
        //        {
        //            Months = i;
        //            break;
        //        }
        //        else if (_DOBDateNow.AddMonths(i) >= ToDate)
        //        {
        //            Months = i - 1;
        //            break;
        //        }
        //    }
        //    YearNum = Years;
        //    int Days = ToDate.Subtract(_DOBDateNow.AddMonths(Months)).Days;
        //    return "" + Years + " năm, " + Months + " tháng, " + Days + " ngày.";
        //    //return $"Age is {_Years} Years {_Months} Months {Days} Days";
        //}
    }
}
