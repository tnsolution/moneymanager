﻿using ComponentFactory.Krypton.Toolkit;
using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Windows.Forms;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Main : Form
    {
        #region ----- Biến toàn cục form -----
        private int _Exit = 0;
        #endregion
        public int _WareHouse = 0;
        public string _WareHouseRole = "";


        public Frm_Main()
        {
            InitializeComponent();
            btn_CategoryReceipt.Click += Btn_CategoryReceipt_Click;

            this.DoubleBuffered = true;

            dte_WorkDate.Validated += Dte_WorkDate_ValueChanged;


            btn_Exit.Click += btn_Exit_Click;
            btn_ChangePass.Click += btn_ChangePass_Click;
            btn_Chi.Click += Btn_Chi_Click;


           
        }

       

        private void Frm_Main_Load(object sender, EventArgs e)
        {

            dte_WorkDate.Value = DateTime.Now;
            Frm_Login frm = new Frm_Login();
            frm.ShowDialog();
            //timer1.Start();
            if (SessionUser.UserLogin != null)
            {
                lbl_EmployeeName.Text = "Xin chào! " + SessionUser.UserLogin.EmployeeName;
            }
            //CheckRole();

            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }
        private void Btn_Chi_Click(object sender, EventArgs e)
        {
            Frm_PhieuChi frm = new Frm_PhieuChi();
            frm.Show();

        }

        #region ----- Panel Left -----
        private void Btn_CategoryReceipt_Click(object sender, EventArgs e)
        {
            Frm_CategoryReceipt frm = new Frm_CategoryReceipt();
            frm.ShowDialog();
        }
        #endregion


        #region Event Others
        void CheckRole()
        {
            btn_ChangePass.Enabled = false;

            #region[Panel_Left]          
            btn_Warehouse_1.Enabled = false;

            btn_Human.Enabled = false;
            btn_Production.Enabled = false;
            btn_Setup.Enabled = false;
            btn_Report.Enabled = false;
            btn_Salary.Enabled = false;
            #endregion


            if (SessionUser.UserLogin == null)
            {
                btn_Exit.Text = "Đăng nhập";
                return;
            }



            if (SessionUser.UserLogin.Key.Length > 0)
            {
                btn_ChangePass.Enabled = true;
                lbl_EmployeeName.Text = "Xin chào! " + SessionUser.UserLogin.EmployeeName;
                btn_Exit.Text = "Đăng xuất";
            }
            else
            {
                btn_Exit.Text = "Đăng nhập";
            }
        }
        private void Dte_WorkDate_ValueChanged(object sender, EventArgs e)
        {
            SessionUser.Date_Work = dte_WorkDate.Value;
        }
        #endregion
        private void btn_Exit_Click(object sender, EventArgs e)
        {
            if (SessionUser.UserLogin != null && SessionUser.UserLogin.Key!="")
            {
                if (Utils.TNMessageBox("Bạn có muốn thoát phần mềm !", 2) == "Y")
                {
                    User_Info zUser = new User_Info(SessionUser.UserLogin.Name);
                    zUser.UpdateIsLogin(0);// hạ cờ đăng nhập
                    Log_Login(3, "Đã đăng xuất phần mềm");
                    new SessionUser();
                    CheckRole();
                    Frm_Login frm = new Frm_Login();
                    frm.ShowDialog();
                    lbl_EmployeeName.Text = "Xin chào! " + SessionUser.UserLogin.EmployeeName;

                }
            }
            else
            {
                new SessionUser();
                CheckRole();
                Frm_Login frm = new Frm_Login();
                frm.ShowDialog();
                lbl_EmployeeName.Text = "Xin chào! " + SessionUser.UserLogin.EmployeeName;

            }
        }
        private void btn_ChangePass_Click(object sender, EventArgs e)
        {
            OpenForm("Frm_ChangePass", false, btn_ChangePass.Tag);
        }


        #region [Open Quick]
        private bool CheckOpened(string name)
        {
            FormCollection fc = Application.OpenForms;

            foreach (Form frm in fc)
            {
                if (frm.Name == name)
                {
                    frm.BringToFront();
                    return true;
                }
            }

            return false;
        }
        /// <summary>
        /// Mở nhanh form
        /// </summary>
        /// <param name="Name">truyền ID của form</param>
        /// <param name="AllowMulti">Cho phép mở nhiều form hay không</param>
        private void OpenForm(string Name, bool AllowMulti,object Tag)
        {
            string zTag = "";
            if (Tag == null)
                zTag = "";
            else
                zTag = Tag.ToString();
            var obj = Activator.CreateInstance(Type.GetType("TNS.WinApp." + Name));
            if (obj != null)
            {
                if (AllowMulti)
                {
                    var form = (Form)obj;
                    form.Tag = zTag;
                    form.Show();
                }
                else
                {
                    if (!CheckOpened(Name))
                    {
                        var form = (Form)obj;
                        form.Tag = zTag;
                        form.Show();
                    }
                }
            }
            else
            {
               Utils.TNMessageBoxOK("Không tìm thấy: " + Name, 1);
            }
        }
        #endregion

        #region [Dùng kéo rê form]

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {

            if (Utils.TNMessageBox("Bạn có muốn thoát phần mềm !", 2) == "Y")
            {
                _Exit = 1;
                if (SessionUser.UserLogin != null && SessionUser.UserLogin.Key != "")
                {
                    User_Info zUser = new User_Info(SessionUser.UserLogin.Name);
                    zUser.UpdateIsLogin(0);// hạ cờ đăng nhập
                    Log_Login(3, "Đã đăng xuất phần mềm");
                }
                this.Close();
            }
            else
            {
                _Exit = 0;
            }
        }

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        private void Frm_Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_Exit == 0)
                e.Cancel = true;
            else
                e.Cancel = false;
            //if (MessageBox.Show("Bạn có muốn thoát phần mềm !.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
            //{

            //    e.Cancel = false;
            //}
            //else
            //{
            //    e.Cancel = true;
            //}
        }
        #endregion


        public void Log_Login(int TypeLog, string Description)
        {

            if (SessionUser.UserLogin != null)
            {
                User_Info nUserLogin = new User_Info();
                nUserLogin.GetEmployee(SessionUser.UserLogin.Key);


                string zComputerName = "";
                if (Environment.MachineName == null)
                {
                    zComputerName = "Đối tượng bị null";
                }
                else
                {
                    zComputerName = Environment.MachineName;
                }

                string zIpAddress = "";
                if (Dns.GetHostAddresses(Environment.MachineName) == null)
                {
                    zIpAddress = "IP bị null";
                }
                else
                {
                    if (Dns.GetHostAddresses(Environment.MachineName).Length == 1)
                        zIpAddress = Dns.GetHostAddresses(Environment.MachineName)[0].ToString(); //có thể bị lấy nhằm IPV6
                    else
                        zIpAddress = Dns.GetHostAddresses(Environment.MachineName)[1].ToString();
                }
                string zWinVersion = "";
                //if (Environment.OSVersion.VersionString == null)
                //    zWinVersion = "Win version bị null";
                //else
                //    zWinVersion = Environment.OSVersion.VersionString.ToString();

                //string subKey = @"SOFTWARE\Wow6432Node\Microsoft\Windows NT\CurrentVersion";
                //Microsoft.Win32.RegistryKey key;
                string zWinName = "";
                //if (Microsoft.Win32.Registry.LocalMachine != null)
                //{
                //    key = Microsoft.Win32.Registry.LocalMachine;
                //    Microsoft.Win32.RegistryKey skey = key.OpenSubKey(subKey);
                //    zWinName = skey.GetValue("ProductName").ToString();
                //}
                //else
                //{
                //    zWinName = "Winname bị null";
                //}

                //Login_Info zInfo = new Login_Info();
                //zInfo.UserKey = nUserLogin.Key;
                //zInfo.DateLogin = DateTime.Now;
                //zInfo.ComputerLogin = zComputerName;
                //zInfo.IPAddress = zIpAddress;
                //zInfo.WindownsName = zWinName;
                //zInfo.WindownsVersion = zWinVersion;
                ////1: thành công
                ////2:thất bại
                ////3:đăng xuất
                //zInfo.TypeLog = TypeLog;
                //zInfo.Description = Description;
                //zInfo.Create();
            }
        }

        
    }
}