﻿using System;
using System.Net;
using System.Windows.Forms;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Login : Form
    {
        public Frm_Login()
        {
            InitializeComponent();
            //txt_User.Text = "tns";
            //txt_Password.Text = "123456";
            btn_Login.Click += Btn_Login_Click;
            //btn_Cancel.Click += Btn_Cancel_Click;
            for(int i=Application.OpenForms.Count-1;i>=0;i--)
            {
                if(Application.OpenForms[i].Name!="Frm_Main")
                {
                    Application.OpenForms[i].Close();
                }
            }
        }

        private void Btn_Cancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Btn_Login_Click(object sender, EventArgs e)
        {
            string[] zResult = User_Data.CheckUser(txt_User.Text, txt_Password.Text);

            if (zResult[0] == "ERR")
            {
                switch (zResult[1])
                {
                    case "CheckUser_Error01":
                        lbl_Message.Text = "Vui lòng kiểm tra lại tên Đăng Nhập";
                        break;
                    case "CheckUser_Error02":
                        lbl_Message.Text = "Tài khoản này chưa Active";
                        break;
                    case "CheckUser_Error03":
                        lbl_Message.Text = "Tài khoản của bạn đã bị khóa";
                        break;
                    case "CheckUser_Error04":
                        lbl_Message.Text = "Tài khoản này hết hạn";
                        break;
                    case "CheckUser_Error05":
                        lbl_Message.Text = "Vui lòng kiểm tra Mật Khẩu";
                        break;
                    case "CheckUser_Error06":
                        lbl_Message.Text = "Tài khoản đang được sử dụng ở máy khác";
                        break;
                }
                lbl_Message.Visible = true;
            }
            else
            {
                string zUserKey = zResult[1];
                SessionUser zUserLogin = new SessionUser(txt_User.Text, zUserKey);
                this.Close();
            }
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
