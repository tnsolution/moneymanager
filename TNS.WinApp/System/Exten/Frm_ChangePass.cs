﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_ChangePass : Form
    {
        private string _UserKey = "";
        private string _PassWordOld = "";
        public Frm_ChangePass()
        {
            InitializeComponent();
            btn_Change.Click += Btn_Change_Click;
            btnClose.Click += btnClose_Click;

            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;
        }

        public string UserKey
        {
            get
            {
                return _UserKey;
            }

            set
            {
                _UserKey = value;
            }
        }

        private void Frm_ChangePass_Load(object sender, EventArgs e)
        {
           
            _UserKey = SessionUser.UserLogin.Key;
            User_Info zUser = new User_Info();
            zUser.GetEmployee(_UserKey);
            _PassWordOld = zUser.Password;
        }
        private void CheckPassWordOld()
        {
            
            if (_PassWordOld != MyCryptography.HashPass(txt_PasswordOld.Text.Trim()))
            {
                MessageBox.Show("Password cũ không đúng!");
                return;
            }
            if (txt_PasswordNew.Text.Length < 4)
            {
                MessageBox.Show("Mật khẩu mới phải ít nhất 4 kí tự!");
                return;
            }
            if (_PassWordOld == MyCryptography.HashPass(txt_PasswordOld.Text.Trim()))
            {
                string zPasswordNew = MyCryptography.HashPass(txt_PasswordNew.Text.Trim());
                string zReplay = MyCryptography.HashPass(txt_Replay.Text.Trim());
                if(zPasswordNew == zReplay)
                {
                    User_Info zUser = new User_Info();
                    zUser.Key = _UserKey;
                    zUser.Password = txt_PasswordNew.Text.Trim();
                    zUser.UpdatePassWord();
                    if(zUser.Message.Length == 0)
                    {
                        MessageBox.Show("Đổi mật khẩu thành công!");
                        zUser.FailedPasswordAttemptCount = 0;
                        zUser.UpdateFaildPassDefault();
                        this.Close();
                    }
                    if(zUser.Message.Length > 0)
                    {
                        MessageBox.Show("Đổi mật khẩu không thành công!");
                    }
                }
                if(zPasswordNew != zReplay)
                {
                    MessageBox.Show("Mật khẩu mới và xác nhận mật khẩu không đúng!");
                }
            }
        }

        private void Btn_Change_Click(object sender, EventArgs e)
        {
            CheckPassWordOld();
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
