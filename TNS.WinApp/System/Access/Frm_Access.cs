﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Access : Form
    {
        private string _Message_Menu = "";
        private string _Message_Role = "";
        private string _UserKey = "";
        private Access_Menu_Info _Access_Menu;
        private int _MenuKey = 0;

        private DataTable _Table = new DataTable();
        private TreeViewCancelEventHandler checkForCheckedChildren;
        public Frm_Access()
        {
            InitializeComponent();
            Utils.DrawGVStyle(ref GV_Role);
            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;
            treeView1.NodeMouseClick += TreeView1_NodeMouseClick;
            treeView1.AfterCheck += TreeView1_AfterCheck;
            btn_Save.Click += Btn_Save_Click;
            GV_Role.CellPainting += GV_Role_CellPainting;
            GV_Role.CellValueChanged += GV_Role_CellValueChanged;
            GV_Role.CellMouseUp += GV_Role_CellMouseUp;
            btn_Search.Click += Btn_Search_Click;
            GV_Role_Layout();

        }



        private void Frm_Access_Load(object sender, EventArgs e)
        {
           
            Check_RoleForm();

            LoadDataToToolbox.KryptonComboBox(cbo_User, "SELECT CAST(UserKey AS NVARCHAR(50)) AS UserKey,UserName FROM [dbo].[SYS_User] WHERE RecordStatus <> 99 ORDER BY UserName", "");

            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            Access_Menu_Data.AutoInsert_MenuToRole(); //neu co tao them menu se tu tao quyen
        }
        private void showCheckedNodesButton_Click(object sender, EventArgs e)
        {
            // Disable redrawing of treeView1 to prevent flickering 
            // while changes are made.
            //treeView1.BeginUpdate();

            // Collapse all nodes of treeView1.
            //treeView1.CollapseAll();

            // Add the checkForCheckedChildren event handler to the BeforeExpand event.
            //treeView1.BeforeExpand += checkForCheckedChildren;

            // Expand all nodes of treeView1. Nodes without checked children are 
            // prevented from expanding by the checkForCheckedChildren event handler.
            treeView1.ExpandAll();

            // Remove the checkForCheckedChildren event handler from the BeforeExpand 
            // event so manual node expansion will work correctly.
            // treeView1.BeforeExpand -= checkForCheckedChildren;

            // Enable redrawing of treeView1.
            //treeView1.EndUpdate();
        }

        // Prevent expansion of a node that does not have any checked child nodes.
        private void CheckForCheckedChildrenHandler(object sender,
            TreeViewCancelEventArgs e)
        {
            if (!HasCheckedChildNodes(e.Node)) e.Cancel = true;
        }

        // Returns a value indicating whether the specified 
        // TreeNode has checked child nodes.
        private bool HasCheckedChildNodes(TreeNode node)
        {
            if (node.Nodes.Count == 0) return false;
            foreach (TreeNode childNode in node.Nodes)
            {
                if (childNode.Checked) return true;
                // Recursively check the children of the current child node.
                if (HasCheckedChildNodes(childNode)) return true;
            }
            return false;
        }
        private void LoadMenu()
        {
            treeView1.Nodes.Clear();
            //checkForCheckedChildren =
            //    new TreeViewCancelEventHandler(CheckForCheckedChildrenHandler);

            treeView1.CheckBoxes = true;


            Access_Menu_Data.AutoInsert_AccessMenu(_UserKey);//Tạo bổ sung menu nếu có menu mới
            DataTable zMain = Access_Menu_Data.MenuMain(_UserKey);
            DataTable zSub = Access_Menu_Data.MenuSub(_UserKey);
            TreeNode root = new TreeNode("MENU");
            root.Tag = 0;
            root.ImageIndex = 0;
            root.NodeFont = new Font("Tahoma", 9, FontStyle.Bold);
            int CountMain = 1;
            int CountSub = 1;
            root.ExpandAll();
            foreach (DataRow r in zMain.Rows)
            {
                TreeNode child = new TreeNode(CountMain.ToString().PadLeft(2,'0') + " - " + r["MenuName"].ToString(), 1, 1);
                child.Tag = r["MenuKey"].ToString() + ";" + r["MenuKey"].ToString(); // tách ra để phân biệt menucon khi click
                child.NodeFont = new Font("Tahoma", 9, FontStyle.Bold);
                if (r["IsAccess"].ToInt() == 1)
                    child.Checked = true;
                else
                    child.Checked = false;
                root.Nodes.Add(child);
                if (zSub.Rows.Count > 0)
                {
                    DataRow[] Array1 = zSub.Select("[Parent]= '" + r["MenuKey"].ToString() + "'");
                    if (Array1.Length > 0)
                    {
                        CountSub = 1;
                        DataTable zGroup1 = Array1.CopyToDataTable();
                        foreach (DataRow r1 in zGroup1.Rows)
                        {
                            TreeNode child1 = new TreeNode( CountSub.ToString().PadLeft(2, '0') + " - " + r1["MenuName"].ToString(), 2, 2);
                            child1.Tag = r1["MenuKey"].ToString();
                            child1.ForeColor = Color.Blue;
                            if (r1["IsAccess"].ToInt() == 1)
                                child1.Checked = true;
                            else
                                child1.Checked = false;
                            child.Nodes.Add(child1);
                            CountSub++;

                        }
                    }
                }
                CountMain++;
            }
            treeView1.Nodes.Add(root);

            // Set the checked state of one of the nodes to
            // demonstrate the showCheckedNodesButton button behavior.
            //treeView1.Nodes[1].Checked = true;



            // Initialize the form.

        }


        private void Btn_Search_Click(object sender, EventArgs e)
        {

            _UserKey = cbo_User.SelectedValue.ToString();
            LoadMenu();
            Load_AccessBranch();
            GV_Role.Rows.Clear();
        }
        private void Load_AccessBranch()
        {
            string AccessBranch = Access_Menu_Data.Load_AccessBranch(_UserKey);
            if (AccessBranch == "2,4")
                rdo_All.Checked = true;
            if (AccessBranch == "2")
                rdo_GT.Checked = true;
            if (AccessBranch == "4")
                rdo_TT.Checked = true;
        }
        private void TreeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node != null)
            {
                string[] s = e.Node.Tag.ToString().Split(';');
                if (s.Length > 1)
                {
                    _MenuKey = s[1].ToInt();
                    GV_Role_LoadData();
                }
            }
        }
        private void GV_Role_Layout()
        {
            DataGridViewCheckBoxColumn zColumn;

            // Setup Column 
            GV_Role.Columns.Add("No", "STT");
            GV_Role.Columns.Add("Role_Name", "Tên Chức Năng");

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "All";
            zColumn.HeaderText = "Tất cả";
            GV_Role.Columns.Add(zColumn);

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Read";
            zColumn.HeaderText = "Xem";
            GV_Role.Columns.Add(zColumn);

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Add";
            zColumn.HeaderText = "Thêm";
            GV_Role.Columns.Add(zColumn);

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Edit";
            zColumn.HeaderText = "Sửa";
            GV_Role.Columns.Add(zColumn);

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Delete";
            zColumn.HeaderText = "Xóa";
            GV_Role.Columns.Add(zColumn);

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Approve";
            zColumn.HeaderText = "Duyệt";
            GV_Role.Columns.Add(zColumn);

            GV_Role.Columns["No"].Width = 40;
            GV_Role.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV_Role.Columns["Role_Name"].Width = 215;
            GV_Role.Columns["Role_Name"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Role.Columns["Role_Name"].Frozen = true;

            GV_Role.Columns["All"].Width = 70;
            GV_Role.Columns["All"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV_Role.Columns["Read"].Width = 70;
            GV_Role.Columns["Read"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV_Role.Columns["Add"].Width = 70;
            GV_Role.Columns["Add"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV_Role.Columns["Edit"].Width = 70;
            GV_Role.Columns["Edit"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV_Role.Columns["Delete"].Width = 70;
            GV_Role.Columns["Delete"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV_Role.Columns["Approve"].Width = 70;
            GV_Role.Columns["Approve"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

        }
        private void GV_Role_LoadData()
        {
            GV_Role.Rows.Clear();
            Access_Menu_Data.AutoInsert_AccessRole(_UserKey);//Tạo bổ sung role nếu có role mới
            _Table = Access_Menu_Data.List_UserRole(_UserKey, _MenuKey);

            int i = 0;
            foreach (DataRow nRow in _Table.Rows)
            {
                GV_Role.Rows.Add();
                DataGridViewRow nRowView = GV_Role.Rows[i];
                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Tag = nRow["RoleKey"].ToString().Trim();
                nRowView.Cells["Role_Name"].Value = nRow["RoleName"].ToString().Trim();
                nRowView.Cells["Role_Name"].Tag = nRow["RoleID"].ToString().Trim();
                if (nRow["Style"].ToString() == "2")
                {
                    nRowView.Cells["Add"].ReadOnly = true;
                    nRowView.Cells["Edit"].ReadOnly = true;
                    nRowView.Cells["Delete"].ReadOnly = true;
                }
                if (nRow["Style"].ToString() == "1")
                {
                    nRowView.Cells["Approve"].ReadOnly = true;
                }
                if (nRow["RoleAll"].ToString() == "1")
                {
                    nRowView.Cells["All"].Value = true;
                }
                if (nRow["RoleRead"].ToString() == "1")
                {
                    nRowView.Cells["Read"].Value = true;
                }
                if (nRow["RoleAdd"].ToString() == "1")
                {
                    nRowView.Cells["Add"].Value = true;
                }
                if (nRow["RoleEdit"].ToString() == "1")
                {
                    nRowView.Cells["Edit"].Value = true;
                }
                if (nRow["RoleDel"].ToString() == "1")
                {
                    nRowView.Cells["Delete"].Value = true;
                }
                if (nRow["RoleApprove"].ToString() == "1")
                {
                    nRowView.Cells["Approve"].Value = true;
                }
                i++;
            }
        }

        private void GV_Role_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                if (GV_Role.Columns[e.ColumnIndex].Name == "Approve" && GV_Role.Rows[e.RowIndex].Cells["Approve"].ReadOnly == true)
                {
                    e.PaintBackground(e.ClipBounds, true);
                    e.Handled = true;
                }
                if (GV_Role.Columns[e.ColumnIndex].Name == "Add" && GV_Role.Rows[e.RowIndex].Cells["Add"].ReadOnly == true)
                {
                    e.PaintBackground(e.ClipBounds, true);
                    e.Handled = true;
                }
                if (GV_Role.Columns[e.ColumnIndex].Name == "Edit" && GV_Role.Rows[e.RowIndex].Cells["Edit"].ReadOnly == true)
                {
                    e.PaintBackground(e.ClipBounds, true);
                    e.Handled = true;
                }
                if (GV_Role.Columns[e.ColumnIndex].Name == "Delete" && GV_Role.Rows[e.RowIndex].Cells["Delete"].ReadOnly == true)
                {
                    e.PaintBackground(e.ClipBounds, true);
                    e.Handled = true;
                }
            }
        }
        private void GV_Role_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow zRowEdit = GV_Role.Rows[e.RowIndex];
            if (e.ColumnIndex == 2 && e.RowIndex != -1)
            {
                if (Convert.ToBoolean(zRowEdit.Cells[e.ColumnIndex].Value) == true)
                {
                    zRowEdit.Cells["Read"].Value = true;
                    zRowEdit.Cells["Add"].Value = true;
                    zRowEdit.Cells["Edit"].Value = true;
                    zRowEdit.Cells["Delete"].Value = true;
                    zRowEdit.Cells["Approve"].Value = true;
                }
                else
                {
                    zRowEdit.Cells["Read"].Value = false;
                    zRowEdit.Cells["Add"].Value = false;
                    zRowEdit.Cells["Edit"].Value = false;
                    zRowEdit.Cells["Delete"].Value = false;
                    zRowEdit.Cells["Approve"].Value = false;
                }
            }
        }
        private void GV_Role_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            GV_Role.EndEdit();
        }
        private void Save_Role()
        {
            string zMessage = "";
            int n = GV_Role.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                if (GV_Role.Rows[i].Tag != null)
                {
                    Access_Role_Info zAccess_Role = new Access_Role_Info(_UserKey, GV_Role.Rows[i].Tag.ToString());
                    zAccess_Role.UserKey = _UserKey;
                    zAccess_Role.RoleKey = GV_Role.Rows[i].Tag.ToString();
                    zAccess_Role.RoleID = GV_Role.Rows[i].Cells["Role_Name"].Tag.ToString();
                    if (GV_Role.Rows[i].Cells["All"].Value != null)
                    {
                        if (Convert.ToBoolean(GV_Role.Rows[i].Cells["All"].Value) == true)
                        {
                            zAccess_Role.RoleAll = 1;
                        }
                        else
                        {
                            zAccess_Role.RoleAll = 0;
                        }
                    }
                    if (GV_Role.Rows[i].Cells["Read"].Value != null)
                    {
                        if (Convert.ToBoolean(GV_Role.Rows[i].Cells["Read"].Value) == true)
                        {
                            zAccess_Role.RoleRead = 1;
                        }
                        else
                        {
                            zAccess_Role.RoleRead = 0;
                        }
                    }
                    if (GV_Role.Rows[i].Cells["Add"].Value != null)
                    {
                        if (Convert.ToBoolean(GV_Role.Rows[i].Cells["Add"].Value) == true)
                        {
                            zAccess_Role.RoleAdd = 1;
                        }
                        else
                        {
                            zAccess_Role.RoleAdd = 0;
                        }
                    }
                    if (GV_Role.Rows[i].Cells["Edit"].Value != null)
                    {
                        if (Convert.ToBoolean(GV_Role.Rows[i].Cells["Edit"].Value) == true)
                        {
                            zAccess_Role.RoleEdit = 1;
                        }
                        else
                        {
                            zAccess_Role.RoleEdit = 0;
                        }
                    }
                    if (GV_Role.Rows[i].Cells["Delete"].Value != null)
                    {
                        if (Convert.ToBoolean(GV_Role.Rows[i].Cells["Delete"].Value) == true)
                        {
                            zAccess_Role.RoleDel = 1;
                        }
                        else
                        {
                            zAccess_Role.RoleDel = 0;
                        }
                    }
                    if (GV_Role.Rows[i].Cells["Approve"].Value != null)
                    {
                        if (Convert.ToBoolean(GV_Role.Rows[i].Cells["Approve"].Value) == true)
                        {
                            zAccess_Role.RoleApprove = 1;
                        }
                        else
                        {
                            zAccess_Role.RoleApprove = 0;
                        }
                    }
                    zAccess_Role.CreatedBy = SessionUser.UserLogin.Key;
                    zAccess_Role.CreatedName = SessionUser.UserLogin.EmployeeName;
                    zAccess_Role.ModifiedBy = SessionUser.UserLogin.Key;
                    zAccess_Role.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    zAccess_Role.Update();
                    if (zAccess_Role.Message != "20")
                    {
                        zMessage += "";
                    }
                }
            }
            if (zMessage == "")
            {
                zMessage = "20";
            }
            _Message_Role = zMessage;
        }

        private void TreeView1_AfterCheck(object sender, TreeViewEventArgs e)
        {
            string[] s = e.Node.Tag.ToString().Split(';');
            _Access_Menu = new Access_Menu_Info(s[0].ToInt(), _UserKey);
            if (e.Node.Checked == true)
                _Access_Menu.IsAccess = 1;
            else
                _Access_Menu.IsAccess = 0;

            //_Access_Menu.ModifiedBy = SessionUser.UserLogin.Key;
            //_Access_Menu.ModifiedName = SessionUser.UserLogin.EmployeeName;
            _Access_Menu.Update();
        }
        private void Save_Menu()
        {
            string zMessage = "";
            foreach (TreeNode node in treeView1.Nodes)
            {
                string[] s = node.Tag.ToString().Split(';');
                _Access_Menu = new Access_Menu_Info(s[0].ToInt(), _UserKey);
                if (node.Checked == true)
                    _Access_Menu.IsAccess = 1;
                else
                    _Access_Menu.IsAccess = 1;

                _Access_Menu.ModifiedBy = SessionUser.UserLogin.Key;
                _Access_Menu.ModifiedName = SessionUser.UserLogin.EmployeeName;
                _Access_Menu.Update();
                if (_Access_Menu.Message != "20")
                {
                    zMessage += _Access_Menu.Message;
                }
            }


            if (zMessage == "")
            {
                zMessage = "20";
            }
            _Message_Role = zMessage;
        }
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (_UserKey != "")
            {
                if (GV_Role.Rows.Count > 0)
                {
                    Save_Role();
                    if (_Message_Role.Substring(0, 2) == "11" ||
                       _Message_Role.Substring(0, 2) == "20")
                    {
                        string zAccessBranch = "";
                        if (rdo_All.Checked == true)
                            zAccessBranch = "2,4";
                        if (rdo_GT.Checked == true)
                            zAccessBranch = "2";
                        if (rdo_TT.Checked == true)
                            zAccessBranch = "4";
                        Access_Menu_Data.Update_AccessBranch(_UserKey, zAccessBranch,SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeName);
                        Utils.TNMessageBoxOK("Cập nhật thành công",3);
                    }
                    else
                    {
                        Utils.TNMessageBoxOK(_Message_Role, 4);
                    }
                }
                else
                {
                   Utils.TNMessageBoxOK("Không tìm thấy dữ liệu!",1);
                }

            }
            else
            {
                Utils.TNMessageBoxOK("Bạn phải chọn 1 tài khoản!", 1);
            }
        }



        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 0)
            {
                btn_Save.Enabled = false;
            }
           
        }
        #endregion
    }

}
