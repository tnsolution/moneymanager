﻿using C1.Win.C1FlexGrid;
using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Windows.Forms;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_User : Form
    {
        private int _Count = 0;
        private string _Key = "";
        private string _EmployeeKey = "";
        private User_Info _User;
        public Frm_User()
        {
            InitializeComponent();
            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            txt_EmployeeID.Enter += Txt_EmployeeID_Enter;
            txt_EmployeeID.Leave += Txt_EmployeeID_Leave;
            btn_Save.Click += Btn_Save_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_New.Click += Btn_New_Click;
            btn_Search.Click += Btn_Search_Click;
            btn_Reset.Click += Btn_Reset_Click;
            GVData.Click += GVData_List_Click;
            btn_Export.Click += Btn_Export_Click;
        }

        private void Frm_User_Load(object sender, EventArgs e)
        {
            Check_RoleForm();
            LoadDataToToolbox.KryptonTextBox(txt_FullName, "SELECT B.FullName FROM HRM_Employee A LEFT JOIN SYS_Personal B ON B.ParentKey = A.EmployeeKey WHERE A.RecordStatus <> 99 AND B.FullName LIKE N'%" + txt_FullName.Text.Trim() + "%' AND B.ParentKey !=  '" + _EmployeeKey + "' ");
            //LV_LoadData();
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            if (_Key.Length > 0)
            {
                txt_Password.Enabled = false;
                btn_Reset.Enabled = true;
            }
            else
            {
                txt_Password.Enabled = true;
                btn_Reset.Enabled = false;
            }
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
        }

        #region [Process Data]
        private void Save()
        {
            _User = new User_Info();
            _User.GetEmployee(_Key);
            _User.Key = _Key;
            _User.Name = txt_User.Text;
            if (txt_User.Text.Trim() == "tns")
            {
                txt_EmployeeID.Tag = "";
            }
            _User.EmployeeKey = txt_EmployeeID.Tag.ToString();
            _User.Password = txt_Password.Text;
            _User.CreatedOn = dte_CreateOn.Value;
            _User.ExpireDate = dte_ExpireDate.Value;
            if (cbo_Status.SelectedIndex == 1)
            {
                _User.RecordStatus = 51;
            }
            if (cbo_Status.SelectedIndex == 2)
            {
                _User.RecordStatus = 52;
            }
            if (cbo_Status.SelectedIndex == 3)
            {
                _User.RecordStatus = 53;
            }
            
            if (cbo_Islogin.SelectedIndex == 0)
                _User.IsLogin = 1;
            else
                _User.IsLogin = 0;
            _User.CreatedBy = SessionUser.UserLogin.Key;
            _User.CreatedName = SessionUser.UserLogin.EmployeeName;
            _User.ModifiedBy = SessionUser.UserLogin.Key;
            _User.ModifiedName = SessionUser.UserLogin.EmployeeName;

            if (_Key.Length > 0)
                _User.Update();
            else
                _User.Create();
            if (_User.Message.Length == 0)
            {
                Utils.TNMessageBoxOK("Cập nhật thành công", 3);
            }
            else
            {
                Utils.TNMessageBoxOK(_User.Message, 4);
            }
            DisplayData();
        }
        private void LoadData()
        {
            _User = new User_Info();
            _User.GetEmployee(_Key);
            if (_User.Name == "tns")
                txt_FullName.Text = "IT Hỗ trợ";
            else
                txt_FullName.Text = _User.EmployeeName;

            txt_User.Text = _User.Name;
            txt_Password.Text = MyCryptography.HashPass(_User.Password);
            if (_User.RecordStatus == 0)
            {
                cbo_Status.SelectedIndex = 0;
            }
            if (_User.RecordStatus == 51)
            {
                cbo_Status.SelectedIndex = 1;
            }
            if (_User.RecordStatus == 52)
            {
                cbo_Status.SelectedIndex = 2;
            }
            if (_User.RecordStatus == 53)
            {
                cbo_Status.SelectedIndex = 2;
            }
            if (_User.ExpireDate < DateTime.Now)
            {
                cbo_Status.SelectedIndex = 3;
            }
            if (_User.ExpireDate != DateTime.MinValue)
            {
                dte_ExpireDate.Value = _User.ExpireDate;
            }
            else
            {
                dte_ExpireDate.Value = DateTime.Now;
            }
            if (_User.CreatedOn != DateTime.MinValue)
            {
                dte_CreateOn.Value = _User.CreatedOn;
            }
            else
            {
                dte_CreateOn.Value = DateTime.Now;
            }
            if (_User.IsLogin == 0)
                cbo_Islogin.SelectedIndex = 1;
            else
                cbo_Islogin.SelectedIndex = 0;
            lbl_Created.Text = "Tạo bởi:[" + _User.CreatedName + "][" + _User.CreatedOn + "]";
            lbl_Modified.Text = "Chỉnh sửa:[" + _User.ModifiedName + "][" + _User.ModifiedOn + "]";
        }
        #endregion

        #region ----- Right -----

        #region[ListView]
        private void DisplayData()
        {
            DataTable ztb = User_Data.Load_ListView(txt_Search.Text.Trim());
            InitGV_Layout(ztb);
        }
        private void GVData_List_Click(object sender, EventArgs e)
        {
            if (GVData.Rows.Count > 0)
            {
                string zKey = GVData.Rows[GVData.RowSel][7].ToString();
                if (zKey != null || zKey != "")
                {
                    _Key = zKey;
                    LoadData();
                }
                else
                {
                    _Key = "";
                    LoadData();
                }
                _Count = User_Data.Count(_Key);
                if (_Key.Length > 0)
                {
                    txt_Password.Enabled = false;
                    btn_Reset.Enabled = true;
                }
                else
                {
                    txt_Password.Enabled = true;
                    btn_Reset.Enabled = false;
                }
            }

        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Danh sách người dùng.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }
                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }

        }
        void InitGV_Layout(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 8;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            //Row Header
            GVData.Rows[0][0] = "Stt";
            GVData.Rows[0][1] = "Tên đăng nhập";
            GVData.Rows[0][2] = "Ngày kích hoạt";
            GVData.Rows[0][3] = "Ngày hết hạn";
            GVData.Rows[0][4] = "Người sử dụng";
            GVData.Rows[0][5] = "Tình trạng";
            GVData.Rows[0][6] = "Đăng nhập";
            GVData.Rows[0][7] = ""; // ẩn cột này

            //Style         
            //GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.Custom;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVData.Styles.Normal.WordWrap = true;

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                GVData.Rows[rIndex + 1][0] = rIndex + 1;
                GVData.Rows[rIndex + 1][1] = rData[1];
                GVData.Rows[rIndex + 1][2] = rData[2];
                GVData.Rows[rIndex + 1][3] = rData[3];
                GVData.Rows[rIndex + 1][4] = rData[4];

                DateTime zDateTime = DateTime.MinValue;
                if (DateTime.TryParse(rData[3].ToString(), out zDateTime))
                {

                }

                string zStatus = "";
                if (int.Parse(rData[5].ToString()) == 0)
                {
                    zStatus = "Chưa Kích Hoạt";
                    GVData.Rows[rIndex + 1].StyleNew.ForeColor = Color.Orange;
                }
                if (int.Parse(rData[5].ToString()) == 51)
                {
                    zStatus = "Kích Hoạt";
                }
                if (int.Parse(rData[5].ToString()) == 52)
                {
                    zStatus = "Bị Khóa";
                    GVData.Rows[rIndex + 1].StyleNew.ForeColor = Color.Orange;
                }
                if (zDateTime < DateTime.Now)
                {
                    zStatus = "Hết Hạn";
                    GVData.Rows[rIndex + 1].StyleNew.ForeColor = Color.Red;
                }
                GVData.Rows[rIndex + 1][5] = zStatus;

                if (rData[6].ToString() == "0")
                {
                    GVData.Rows[rIndex + 1][6] = "Đăng xuất phần mềm";
                }
                else
                {
                    GVData.Rows[rIndex + 1][6] = "Đăng nhập phần mềm";
                    GVData.Rows[rIndex + 1].StyleNew.ForeColor = Color.Green;
                }

                GVData.Rows[rIndex + 1][7] = rData[0];
            }



            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;
            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            GVData.Rows[0].Height = 50;

            GVData.Cols[0].Width = 30;
            GVData.Cols[1].Width = 150;
            GVData.Cols[2].Width = 100;
            GVData.Cols[3].Width = 100;
            GVData.Cols[4].Width = 200;
            GVData.Cols[5].Width = 120;
            GVData.Cols[6].Width = 50;
            GVData.Cols[7].Visible = false;
        }
        #endregion



        #region [Process Event]
        //private void Txt_FullName_Leave(object sender, EventArgs e)
        //{
        //    if (txt_FullName.Text.Length > 0)
        //    {
        //        Personal_Info zPerson = new Personal_Info(txt_FullName.Text, true, 1);
        //        txt_FullName.Tag = zPerson.ParentKey;
        //    }
        //}
        private void Txt_EmployeeID_Enter(object sender, EventArgs e)
        {

        }
        private void Txt_EmployeeID_Leave(object sender, EventArgs e)
        {
            if (txt_EmployeeID.Text.Trim().Length < 1)
            {
                txt_EmployeeID.Text = "Nhập mã thẻ";
                txt_EmployeeID.ForeColor = Color.White;
            }
            else
            {
                //if (zinfo.Key == "")
                //{
                //    txt_EmployeeID.Text = "Nhập mã thẻ";
                //    txt_EmployeeID.ForeColor = Color.White;
                //    txt_EmployeeID.Tag = null;
                //}
                //else
                //{
                //    txt_EmployeeID.Text = zinfo.EmployeeID;
                //    txt_FullName.Text = zinfo.FullName;
                //    txt_EmployeeID.Tag = zinfo.Key;
                //    txt_EmployeeID.BackColor = Color.Black;
                //}
            }
        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            _Key = "";
            LoadData();
            if (_Key.Length > 0)
            {
                txt_Password.Enabled = false;
                btn_Reset.Enabled = true;
            }
            else
            {
                txt_Password.Enabled = true;
                btn_Reset.Enabled = false;
            }
        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (_Key == "")
            {
               Utils.TNMessageBoxOK("Chưa chọn thông tin!",1);
            }
            else
            {
                if (Utils.TNMessageBox("Bạn có chắc xóa thông tin này ?.",2) == "Y")
                {
                    _User = new User_Info();
                    _User.Key = _Key;
                    _User.Delete();
                }
                DisplayData();
            }
        }
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (txt_User.Text.Trim() == "")
            {
                Utils.TNMessageBoxOK("Vui lòng nhập tên đăng nhập!", 1);
                return;
            }
            else
            {
                Save();
            }
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            DisplayData();
        }
        private void Btn_Reset_Click(object sender, EventArgs e)
        {
            if (_Key.Length == 0)
            {
                txt_Password.Text = "123456Aa@";
            }
            else
            {
                User_Info zUser = new User_Info();
                zUser.Key = _Key;
                zUser.Password = "123456";
                zUser.ResetPassWord();
                if (zUser.Message.Length == 0)
                {
                    MessageBox.Show("Reset mật khẩu người dùng thành công \n - Mật khẩu mới là  : 123456", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        #endregion
        #endregion
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 0)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 1)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = true;
            }
            if (Role.RoleDel == 0)
            {
                btn_Del.Enabled = false;
            }
        }
        #endregion
    }
}
