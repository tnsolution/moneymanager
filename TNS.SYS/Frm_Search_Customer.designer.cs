﻿namespace TN_WinApp
{
    partial class Frm_Search_Customer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Panel_Left_Search = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.cbo_Slug_Customer = new System.Windows.Forms.ComboBox();
            this.txt_Search = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.LV_Search = new System.Windows.Forms.ListView();
            this.Panel_Left_Search.SuspendLayout();
            this.SuspendLayout();
            // 
            // Panel_Left_Search
            // 
            this.Panel_Left_Search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(220)))), ((int)(((byte)(240)))));
            this.Panel_Left_Search.Controls.Add(this.label5);
            this.Panel_Left_Search.Controls.Add(this.cbo_Slug_Customer);
            this.Panel_Left_Search.Controls.Add(this.txt_Search);
            this.Panel_Left_Search.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Left_Search.Location = new System.Drawing.Point(0, 39);
            this.Panel_Left_Search.Name = "Panel_Left_Search";
            this.Panel_Left_Search.Size = new System.Drawing.Size(320, 75);
            this.Panel_Left_Search.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label5.Location = new System.Drawing.Point(12, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 15);
            this.label5.TabIndex = 111;
            this.label5.Text = "Loại KH";
            // 
            // cbo_Slug_Customer
            // 
            this.cbo_Slug_Customer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.cbo_Slug_Customer.FormattingEnabled = true;
            this.cbo_Slug_Customer.Location = new System.Drawing.Point(74, 12);
            this.cbo_Slug_Customer.Name = "cbo_Slug_Customer";
            this.cbo_Slug_Customer.Size = new System.Drawing.Size(239, 21);
            this.cbo_Slug_Customer.TabIndex = 10;
            // 
            // txt_Search
            // 
            this.txt_Search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.txt_Search.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Search.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Search.Location = new System.Drawing.Point(74, 47);
            this.txt_Search.Name = "txt_Search";
            this.txt_Search.Size = new System.Drawing.Size(239, 22);
            this.txt_Search.TabIndex = 0;
            this.txt_Search.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_Search_KeyUp);
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(199)))), ((int)(((byte)(231)))));
            this.label6.Dock = System.Windows.Forms.DockStyle.Top;
            this.label6.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(1)))), ((int)(((byte)(0)))));
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(320, 39);
            this.label6.TabIndex = 5;
            this.label6.Text = "CHỌN KHÁCH HÀNG";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LV_Search
            // 
            this.LV_Search.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LV_Search.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LV_Search.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_Search.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.LV_Search.FullRowSelect = true;
            this.LV_Search.GridLines = true;
            this.LV_Search.Location = new System.Drawing.Point(0, 114);
            this.LV_Search.Name = "LV_Search";
            this.LV_Search.Size = new System.Drawing.Size(320, 261);
            this.LV_Search.TabIndex = 6;
            this.LV_Search.UseCompatibleStateImageBehavior = false;
            this.LV_Search.View = System.Windows.Forms.View.Details;
            this.LV_Search.ItemActivate += new System.EventHandler(this.LV_Search_ItemActivate);
            this.LV_Search.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LV_Search_KeyDown);
            // 
            // Frm_Search_Customer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(320, 375);
            this.Controls.Add(this.LV_Search);
            this.Controls.Add(this.Panel_Left_Search);
            this.Controls.Add(this.label6);
            this.Name = "Frm_Search_Customer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Frm_Search_Customer";
            this.Load += new System.EventHandler(this.Frm_Search_Customer_Load);
            this.Panel_Left_Search.ResumeLayout(false);
            this.Panel_Left_Search.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Panel_Left_Search;
        private System.Windows.Forms.TextBox txt_Search;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ListView LV_Search;
        private System.Windows.Forms.ComboBox cbo_Slug_Customer;
        private System.Windows.Forms.Label label5;
    }
}