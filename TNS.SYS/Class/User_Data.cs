﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
using TNS.Misc;

namespace TNS.SYS
{
    public class User_Data
    {
        public static string[] CheckUser(string UserName, string Pass)
        {
            string[] nResult = new string[2];
            User_Info nUserLogin = new User_Info(UserName);
            int zFailPass = User_Data.CountFailedPass(nUserLogin.Key);
            if (nUserLogin.Key.Trim().Length == 0)
            {
                nResult[0] = "ERR";
                nResult[1] = "CheckUser_Error01";
                return nResult;//"Don't have this UserName";
            }

            if (nUserLogin.RecordStatus == 52)
            {
                nResult[0] = "ERR";
                nResult[1] = "CheckUser_Error02";
                return nResult;//"Don't Activate"
            }

            if (nUserLogin.RecordStatus == 53)
            {
                nResult[0] = "ERR";
                nResult[1] = "CheckUser_Error03";
                return nResult;//"User IsLock"
            }

            if (nUserLogin.ExpireDate < DateTime.Now)
            {
                nResult[0] = "ERR";
                nResult[1] = "CheckUser_Error04";
                return nResult;//"Expire On"
            }

            if (nUserLogin.Password != MyCryptography.HashPass(Pass))
            {
                nUserLogin.UpdateFailedPass();
                nResult[0] = "ERR";
                nResult[1] = "CheckUser_Error05";
                nUserLogin.UpdateFailedPass();
                nResult[0] = "ERR";
                nResult[1] = "CheckUser_Error05";

                if (zFailPass > 3)
                {
                    string _UserKey = nUserLogin.Key;
                    nUserLogin = new User_Info();
                    nUserLogin.Key = _UserKey;
                    nUserLogin.RecordStatus = 53;
                    nUserLogin.FailedPasswordAttemptCount = 0;
                    nUserLogin.UpdateIsLock();
                }
                return nResult;// "Wrong Password";
            }
            if (nUserLogin.IsLogin == 1 && UserName.ToUpper() != "TNS")
            {
                nResult[0] = "ERR";
                nResult[1] = "CheckUser_Error06";
                return nResult;
            }
            nResult[0] = "OK";
            nResult[1] = nUserLogin.Key;
            nUserLogin.UpdateDateLogin();
            //nUserLogin.UpdateIsLogin(1);// bật cờ đã đăng nhập
            return nResult;
        }

        public static DataTable GetUserRole(string UserKey, string RoleID)
        {
            DataTable zTable = new DataTable();
            string zSQL = "[dbo].[SYS_Users_Roles_SELECT]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(UserKey);
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = RoleID;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable GetUserFormRole(string UserKey, string RoleID)
        {
            DataTable zTable = new DataTable();
            string zSQL = "[dbo].[SYS_Users_Roles_Form_SELECT]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(UserKey);
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = RoleID;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        #region [UserRole Add-Edit-Delete-Send-Read]
        public static DataTable GetUserEdit(string UserKey, string RoleID)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT [dbo].[SYS_CheckRole_Edit] (@UserKey,@RoleID)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(UserKey);
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = RoleID;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable GetUserAdd(string UserKey, string RoleID)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT [dbo].[SYS_CheckRole_Add] (@UserKey,@RoleID)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(UserKey);
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = RoleID;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable GetUserDel(string UserKey, string RoleID)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT [dbo].[SYS_CheckRole_Del] (@UserKey,@RoleID)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(UserKey);
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = RoleID;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable GetUserSend(string UserKey, string RoleID)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT [dbo].[SYS_CheckRole_Send] (@UserKey,@RoleID)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(UserKey);
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = RoleID;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable GetUserApprove(string UserKey, string RoleID)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT [dbo].[SYS_CheckRole_Approve] (@UserKey,@RoleID)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(UserKey);
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = RoleID;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static int GetUserRoleRead(string UserKey, string RoleID)
        {
            int zResult = 0;
            string zSQL = @"SELECT [dbo].[SYS_CheckRole_Read] (@UserKey,@RoleID)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(UserKey);
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = RoleID;
                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zResult;
        }
        public static DataTable GetUserRoleIsAccess(string UserKey, string MenuID)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT [dbo].[SYS_CheckRole_IsAccess](@UserKey,@MenuID)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                zCommand.Parameters.Add("@MenuID", SqlDbType.NVarChar).Value = MenuID;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        #endregion

        public static DataTable GetUserRoleForAdd(string UserKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = " SELECT * FROM [dbo].[SYS_Roles] WHERE RoleKey NOT IN "
                        + " (SELECT RoleKey FROM[SYS_Users_Roles] "
                        + " WHERE UserKey = @UserKey "
                        + " GROUP BY RoleKey)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(UserKey);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static string UserRoleInsert(string UserKey, string RoleID, string Set_UserKey, string Set_RoleKey)
        {
            string zSQL = "[dbo].[SYS_Users_Roles_INSERT]";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@Set_UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(Set_UserKey);
                zCommand.Parameters.Add("@Set_RoleKey", SqlDbType.UniqueIdentifier).Value = new Guid(Set_RoleKey);

                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(UserKey);
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = RoleID;

                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                zResult = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public static string UserRoleUpdate(string UserKey, string RoleID, string Set_UserKey, string Set_RoleKey, int CategoryID)
        {
            string zSQL = "[dbo].[SYS_Users_Roles_UPDATE]";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@Set_UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(Set_UserKey);
                zCommand.Parameters.Add("@Set_RoleKey", SqlDbType.UniqueIdentifier).Value = new Guid(Set_RoleKey);
                zCommand.Parameters.Add("@CategoryID", SqlDbType.NVarChar).Value = CategoryID;

                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(UserKey);
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = RoleID;

                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                zResult = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public static DataTable UserRole_ID(string UserKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.*, b.RoleID
FROM dbo.SYS_Users_Roles A
LEFT JOIN dbo.SYS_Roles B ON B.RoleKey = A.RoleKey
WHERE UserKey = @UserKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(UserKey);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Load_ListView(string UserName)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.UserKey,A.UserName,A.CreatedOn, A.ExpireDate,C.FullName,A.RecordStatus,A.IsLogin FROM [dbo].[SYS_User] A
LEFT JOIN [dbo].[HRM_Employee] B ON A.EmployeeKey = B.EmployeeKey 
LEFT JOIN [dbo].[SYS_Personal] C  ON B.EmployeeKey = C.ParentKey  
WHERE A.UserName != N'admin' AND A.RecordStatus <> 99";
            if (UserName.Length > 0)
            {
                zSQL += " AND A.UserName LIKE @UserName OR C.FullName LIKE @UserName";
            }

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = "%" + UserName + "%"; ;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static int Count(string UserKey)
        {
            int result = 0;
            string zSQL = @"SELECT COUNT(*) FROM [dbo].[SYS_Users_Roles] WHERE UserKey = @UserKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(UserKey);
                result = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return result;
        }
        public static int CountUser(string UserKey)
        {
            int result = 0;
            string zSQL = @"SELECT COUNT(*) FROM [dbo].[SYS_User] WHERE UserKey = @UserKey AND RecordStatus <> 99";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(UserKey);
                result = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return result;
        }
        public static DataTable Role()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SYS_Roles WHERE CategoryKey  = 1 AND RoleID NOT LIKE  N'MSYS01'";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Role_Form()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SYS_Roles WHERE CategoryKey  = 2";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Users()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT UserKey,UserName FROM SYS_User WHERE RecordStatus  = 51 AND UserName != 'Admin' AND RecordStatus <> 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Get_User_Menu(string UserKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"[dbo].[SYS_Access_Menu_Data_WinForm]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Get_User_Role(string UserKey, string MenuID)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"[dbo].[SYS_Access_Role_Data_WinForm]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = MenuID;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable User(string UserName)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT UserKey,UserName FROM SYS_User WHERE UserName LIKE N'%" + UserName + "%'  AND RecordStatus <> 99 ORDER BY UserName";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static int CountFailedPass(string UserKey)
        {
            int result = 0;
            string zSQL = @"SELECT FailedPasswordAttemptCount FROM SYS_User WHERE UserKey = @UserKey AND RecordStatus <> 99";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(UserKey);
                result = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return result;
        }

    }
}
