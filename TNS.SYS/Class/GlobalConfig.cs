﻿using System.Globalization;
namespace TNS.WinApp
{
    public class GlobalConfig
    {
        private static CultureInfo _Provider;
        private static string _String_Format_Currency = "#,###,##0";
        //4    
        private static string _String_Format_Decimal = "#,###,##0";                 //5
        private static string mFormatDate = "";                     //7
     
        private static string m_Message = "";
        public GlobalConfig()
        {
            _Provider = new CultureInfo("de-DE");
        }
        public void LoadGlobalSystemConfig()
        {
            
        }
        public static CultureInfo Provider
        {
            get
            {
                return _Provider;
            }

        }
        public static string String_Format_Currency
        {
            get
            {
                return _String_Format_Currency;
            }
        }
        public static string String_Format_Decimal
        {
            get
            {
                return _String_Format_Decimal;
            }
        }
       
    }
}
