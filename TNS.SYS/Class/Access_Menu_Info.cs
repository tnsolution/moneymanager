﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.SYS
{
    public class Access_Menu_Info
    {
        #region [ Field Name ]
        private string _UserKey = "";
        private int _MenuKey = 0;
        private int _IsAccess = 0;
        private int _Rank = 0;
        private int _RecordStatus = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public bool IsCommandOK
        {
            get
            {
                int zNumber = 0;
                int.TryParse(_Message.Substring(0, 1), out zNumber);
                if (zNumber <= 3)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
       
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public string UserKey
        {
            get { return _UserKey; }
            set { _UserKey = value; }
        }
        public int MenuKey
        {
            get { return _MenuKey; }
            set { _MenuKey = value; }
        }
        public int IsAccess
        {
            get { return _IsAccess; }
            set { _IsAccess = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Access_Menu_Info()
        {
        }
        public Access_Menu_Info(int MenuKey, string UserKey)
        {
            string zSQL = "SELECT * FROM SYS_Access_Menu WHERE MenuKey = @MenuKey AND UserKey = @UserKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@MenuKey", SqlDbType.Int).Value = MenuKey;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _UserKey = zReader["UserKey"].ToString();
                    
                    if (zReader["MenuKey"] != DBNull.Value)
                    {
                        _MenuKey = int.Parse(zReader["MenuKey"].ToString());
                    }
                    if (zReader["IsAccess"] != DBNull.Value)
                    {
                        _IsAccess = int.Parse(zReader["IsAccess"].ToString());
                    }

                    if (zReader["Rank"] != DBNull.Value)
                    {
                        _Rank = int.Parse(zReader["Rank"].ToString());
                    }

                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "SYS_Access_Menu_INSERT";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@MenuKey", SqlDbType.Int).Value = _MenuKey;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey;
                zCommand.Parameters.Add("@IsAccess", SqlDbType.Int).Value = _IsAccess;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "SYS_Access_Menu_UPDATE";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@MenuKey", SqlDbType.Int).Value = _MenuKey;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey;
                zCommand.Parameters.Add("@IsAccess", SqlDbType.Int).Value = _IsAccess;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "SYS_Access_Menu_DELETE";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@MenuKey", SqlDbType.Int).Value = _MenuKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_MenuKey == 0 &&
                _UserKey.Trim().Length == 0)
            {
                zResult = Create();
            }
            else
            {
                zResult = Update();
            }

            return zResult;
        }
        #endregion
    }
}
