﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
using TNS.Misc;

namespace TNS.SYS
{
    public class Access_Menu_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM SYS_Access_Menu WHERE RecordStatus <> 99 ORDER BY [RANK]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

        public static DataTable List(string UserKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SYS_Access_Menu_Data";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
               
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static int IsAccess(string Url)
        {
            int zResult = 0;
            string zSQL = @"SELECT A.IsAccess
FROM SYS_User_Access_Menu A 
LEFT JOIN SYS_Menu B ON A.MenuKey = B.MenuKey
WHERE A.RecordStatus <> 99 
AND B.MenuLink = @Url";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Url", SqlDbType.NVarChar).Value = Url;
                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zResult;
        }

        public static int Find_User(string UserKey,int MenuKey)
        {
            int zResult = 0;
            string zSQL = @"SELECT Count(*) FROM SYS_Access_Menu WHERE UserKey = @UserKey AND MenuKey = @MenuKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                zCommand.Parameters.Add("@MenuKey", SqlDbType.Int).Value = MenuKey;
                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zResult;
        }

        //phân quyền đông làm

        public static void AutoInsert_MenuToRole()
        {
            int zResult = 0;
            string zSQL = @"
                             INSERT INTO [dbo].[SYS_Role]
                            (RoleKey,RoleID,RoleName, Style,Module,Parent,[Rank],RecordStatus)
                            SELECT newid(),MenuID,MenuName,Type,Module,Parent,[Rank],0 FROM   [dbo].[SYS_Menu]
		                            WHERE   MenuID NOT IN 
			                            (
			                            SELECT RoleID FROM  [dbo].[SYS_Role]
			                            )
                            AND MenuLevel!=0";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
        }
        //Insert cho đủ quyền nếu có thêm quyền menu
        public static void AutoInsert_AccessMenu(string UserKey)
        {
            int zResult = 0;
            string zSQL = @"
                             INSERT INTO [dbo].[SYS_Access_Menu]
                             (UserKey,MenuKey,IsAccess, RecordStatus)
                             SELECT @UserKey, MenuKey, 0,0 FROM   [dbo].[SYS_Menu]
		                     WHERE   MenuKey NOT IN 
			                    (
			                    SELECT MenuKey FROM  [dbo].[SYS_Access_Menu]
			                    WHERE UserKey = @UserKey
			                    ) 
			                    AND RecordStatus <>99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
        }
        public static DataTable MenuMain(string UserKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.MenuKey,B.MenuName,A.IsAccess  FROM SYS_Access_Menu A
LEFT JOIN[dbo].[SYS_Menu] B ON B.MenuKey=A.MenuKey
WHERE B.RecordStatus<> 99 AND MenuLevel = 0
AND A.UserKey= @UserKey
ORDER BY B.Rank
";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable MenuSub(string UserKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.MenuKey,B.MenuName,A.IsAccess,B.Parent  FROM SYS_Access_Menu A
LEFT JOIN[dbo].[SYS_Menu] B ON B.MenuKey=A.MenuKey
WHERE B.RecordStatus<> 99 AND MenuLevel = 1
AND A.UserKey= @UserKey
ORDER BY B.Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable Get_User_Role(string UserKey, string MenuID)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"[dbo].[SYS_Access_Role_Data_WinForm]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = MenuID;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static void AutoInsert_AccessRole(string UserKey)
        {
            int zResult = 0;
            string zSQL = @"
                              INSERT INTO [dbo].[SYS_Access_Role]
                                (UserKey,RoleKey,RoleID, RoleAll,  RoleRead,RoleAdd,RoleEdit,RoleDel,RoleApprove, RecordStatus)
                                SELECT @UserKey, RoleKey,RoleID,0,0,0,0,0,0,0 FROM   [dbo].[SYS_Role]
                                WHERE   RoleKey NOT IN 
                                (
                                SELECT RoleKey FROM  [dbo].[SYS_Access_Role]
                                WHERE UserKey = @UserKey
                                ) 
                                AND RecordStatus <>99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
        }
        public static DataTable List_UserRole(string UserKey, int MenuKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT B.RoleName,B.RoleKey,B.RoleID, A.UserKey,B.Style,A.RoleAll, A.RoleRead,A.RoleAdd,A.RoleEdit,A.RoleApprove,A.RoleDel
                            FROM SYS_Access_Role A 
                            LEFT JOIN SYS_Role B ON A.RoleKey = B.RoleKey
                            WHERE UserKey = @UserKey AND B.Parent = @Parent AND B.RecordStatus != 99 ORDER BY B.Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = MenuKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static string Load_AccessBranch(string UserKey)
        {
            string zResult = "";
            string zSQL = @" DECLARE @Count INT =0
SELECT @Count =Count(*) FROM SYS_Access_Branch WHERE UserKey = @UserKey
IF (@Count = 0)
    INSERT INTO [dbo].[SYS_Access_Branch] (UserKey,BranchKey,RecordStatus)
    VALUES (@UserKey,'4',0)
SELECT  BranchKey FROM SYS_Access_Branch WHERE UserKey = @UserKey

";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zResult;
        }
        public static void Update_AccessBranch(string UserKey,string BranchKey, string ModifiedBy, string ModifiedName)
        {
            string zSQL = @"
    UPDATE  [dbo].[SYS_Access_Branch] SET UserKey =@UserKey,BranchKey =@BranchKey,ModifiedBy=@ModifiedBy,ModifiedName =@ModifiedName
    WHERE UserKey = @UserKey

";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = BranchKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = ModifiedName;
                zCommand.ExecuteNonQuery();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
        }
    }
}
