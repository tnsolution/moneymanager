﻿using System;
using System.Data;

namespace TNS.SYS
{
    public class SessionUser
    {
        private static User_Info _UserLogin;
        private static DataTable _Table_Role;
        private static DateTime _Date_Work;

        public static User_Info UserLogin
        {
            get
            {
                return _UserLogin;
            }

        }
       
        public static DataTable Table_Role
        {
            get
            {
                return _Table_Role;
            }

            set
            {
                _Table_Role = value;
            }
        }
        public static DateTime Date_Work
        {
            get
            {
                return _Date_Work;
            }

            set
            {
                _Date_Work = value;
            }
        }

        public SessionUser()
        {
            _UserLogin = new User_Info();
            _Table_Role = new DataTable();
            _Date_Work = DateTime.Now;
        }
        public SessionUser(string UserName)
        {
            _UserLogin = new User_Info(UserName);
        }
        public SessionUser(string UserName, string UserKey)
        {
            _UserLogin = new User_Info(UserName);
            
            Table_Role = User_Data.UserRole_ID(UserKey);
            Date_Work = DateTime.Now;
        }
    }
}
