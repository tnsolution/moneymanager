﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TN.Library.System;
using TN.Library.SystemManagement;

namespace TN_WinApp
{
    public partial class Frm_Branch : Form
    {
        #region[Private]
        private int _Key = 0;
        #endregion
        public Frm_Branch()
        {
            InitializeComponent();
            btn_Save.Click += Btn_Save_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_Search.Click += Btn_Search_Click;
            LV_List.Click += LV_List_Click;
        }
        private void Frm_Branch_Load(object sender, EventArgs e)
        {
            LV_Layout(LV_List);
            LV_LoadData();
        }
        #region[Progess]

        private void LoadData()
        {

           Branch_Info zinfo = new Branch_Info(_Key);
            txt_BranchID.Text = zinfo.BranchID;
            txt_BranchName.Text = zinfo.BranchName;
            txt_Rank.Text = zinfo.Rank.ToString();
            txt_Description.Text = zinfo.Description;
        }

        private void SetDefault()
        {
            _Key = 0;
            txt_BranchID.Text = "";
            txt_BranchName.Text = "";
            txt_Rank.Text = "0";
            txt_Description.Text = "";
        }

        #endregion

        #region[Event]
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (txt_BranchID.Text.Trim().Length == 0)
            {
                MessageBox.Show("Chưa nhập mã!");
                return;
            }
            if (txt_BranchName.Text.Trim().Length == 0)
            {
                MessageBox.Show("Chưa nhập tên chức vụ!");
                return;
            }
            int zRank;
            if (!int.TryParse(txt_Rank.Text, out zRank))
            {
                MessageBox.Show("Vui lòng nhập đúng định dạng số!");
                return;
            }
            else
            {
               Branch_Info zinfo = new Branch_Info(_Key);
                zinfo.BranchID = txt_BranchID.Text.Trim().ToUpper();
                zinfo.BranchName = txt_BranchName.Text.Trim();
                zinfo.Rank = int.Parse(txt_Rank.Text.Trim());
                zinfo.Description = txt_Description.Text.Trim();
                zinfo.Save();
                if (zinfo.Message.Substring(0, 2) == "20" || zinfo.Message.Substring(0, 2) == "11")
                {
                    MessageBox.Show("Cập nhật thành công!");
                    LV_LoadData();
                    SetDefault();
                }
                else
                {
                    MessageBox.Show("Cập nhật không thành công!Vui lòng kiểm tra lại!");
                }
            }


        }

        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (_Key == 0)
            {
                MessageBox.Show("Chưa chọn thông tin!");
            }
            else
            {
                if (MessageBox.Show("Bạn có chắc xóa thông tin này ?.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                   Branch_Info zinfo = new Branch_Info(_Key);
                    zinfo.Delete();
                    if (zinfo.Message.Substring(0, 2) == "30")
                    {
                        MessageBox.Show("Đã xóa !");
                        LV_LoadData();
                        SetDefault();
                    }
                    else
                    {
                        MessageBox.Show("Xóa không thành công!Vui lòng kiểm tra lại!");
                    }

                }
            }

        }


        private void Btn_New_Click(object sender, EventArgs e)
        {
            SetDefault();
        }

        private void Btn_Search_Click(object sender, EventArgs e)
        {

            DataTable zTable = Branch_Data.Search(txt_Search.Text.Trim());
            LV_LoadDataSearch(zTable);
        }
        #endregion


        #region[ListView]
        private void LV_Layout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 30;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên chi nhánh";
            colHead.Width = 195;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "Diễn giải";
            //colHead.Width = 270;
            //colHead.TextAlign = HorizontalAlignment.Left;
            //LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "Sắp xếp ";
            //colHead.Width = 100;
            //colHead.TextAlign = HorizontalAlignment.Right;
            //LV.Columns.Add(colHead);
        }
        private void LV_LoadData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_List;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            LV.Items.Clear();
            DataTable ztb = Branch_Data.List();
            for (int i = 0; i < ztb.Rows.Count; i++)
            {
                DataRow nRow = ztb.Rows[i];

                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.Tag = nRow["BranchKey"];
                lvi.ForeColor = Color.Navy;

                lvi.BackColor = Color.White;
                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["BranchID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["BranchName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                //lvsi = new ListViewItem.ListViewSubItem();
                //lvsi.Text = nRow["Description"].ToString().Trim();
                //lvi.SubItems.Add(lvsi);

                //lvsi = new ListViewItem.ListViewSubItem();
                //lvsi.Text = nRow["Rank"].ToString().Trim();
                //lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;

        }

        private void LV_LoadDataSearch(DataTable In_Table)
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_List;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            LV.Items.Clear();
            for (int i = 0; i < In_Table.Rows.Count; i++)
            {
                DataRow nRow = In_Table.Rows[i];

                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.Tag = nRow["BranchKey"];
                lvi.ForeColor = Color.Navy;

                lvi.BackColor = Color.White;
                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["BranchID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["BranchName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                //lvsi = new ListViewItem.ListViewSubItem();
                //lvsi.Text = nRow["Description"].ToString().Trim();
                //lvi.SubItems.Add(lvsi);

                //lvsi = new ListViewItem.ListViewSubItem();
                //lvsi.Text = nRow["Rank"].ToString().Trim();
                //lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;

        }




        #endregion
        private void LV_List_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < LV_List.Items.Count; i++)
            {
                if (LV_List.Items[i].Selected == true)
                {
                    LV_List.Items[i].BackColor = Color.LightBlue; // highlighted item
                }
                else
                {
                    LV_List.Items[i].BackColor = SystemColors.Window; // normal item
                }
            }
            _Key = int.Parse(LV_List.SelectedItems[0].Tag.ToString());
            LoadData();
        }

    }
}
